package main

import "fmt"

func updateInsertedValuesPositions(array *[]int, holePosition *int, valueToInsert int) {
	for *holePosition > 0 && (*array)[*holePosition-1] > valueToInsert {
		(*array)[*holePosition] = (*array)[*holePosition-1]
		*holePosition = *holePosition - 1
		fmt.Println("Validation way of hole Position: ", *holePosition)
	}
}

func insertionSorting(array []int) {
	holePosition := 0
	for index, value := range array {
		holePosition = index
		fmt.Printf("step: %d, values: %d\n", index, array)
		updateInsertedValuesPositions(&array, &holePosition, value)
		array[holePosition] = value
	}
	fmt.Println("Final: ", array)
}

func main() {
	insertionSorting([]int{14, 33, 27, 10, 35, 19, 42, 44})
}
