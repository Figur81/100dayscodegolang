<div align="center">
  <a href="http://ant.design">
    <img width="400" src="https://d9hhrg4mnvzow.cloudfront.net/join.teamtreehouse.com/100-days-of-code/8c61d5fd-100daysofcode-badgefinals-05_107m07m000000000000028.png">
  </a>
  <h1> 100DaysofCode </h1>

  Coding 1 hour during 100 days!
</div>

## What's 100 Days code?
[\#100DaysofCode](https://www.100daysofcode.com) is a developer challenge that we code during 100 days during 1 hour each day

## 🤝 Contributing!

Fell free to contributing using:  
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

## Contact

If you wanna talk about this experience or follow my evolution during these 100 days, follow me on [twitter](https://twitter.com/figur7Plus1)

## What I'm seeing to help me in this journey?
### Articles
* English
    * Day 2
        * [Data Structure and Algorithms Linear Search](https://www.tutorialspoint.com/data_structures_algorithms/linear_search_algorithm.htm)
    * Day 3
        * [Data Structure - Interpolation Search](https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm)
    * Day 4
        * [rabbitmq tutorial](https://www.rabbitmq.com/tutorials/tutorial-one-go.html)
        * [RabbitMQ in 5 minutes](https://www.youtube.com/watch?v=deG25y_r6OY)
    * Day 5
        * [Data Structure and Algorithms - Hash Table](https://www.tutorialspoint.com/data_structures_algorithms/hash_data_structure.htm)
        * [Introduction to Hash Table in Go Programming](https://monkelite.com/introduction-to-hash-table-in-go-programming/)
        * [Data Structures: Hash Tables](https://www.youtube.com/watch?v=shs0KM3wKv8)
    * Day 6
        * [Data Structure - Bubble Sort Algorithm](https://www.tutorialspoint.com/data_structures_algorithms/bubble_sort_algorithm.htm)
        * [Implementing the Bubble Sort Algorithm using Golang](https://tutorialedge.net/golang/implementing-bubble-sort-with-golang/)
    * Day 7
        * [Data Structure and Algorithms Insertion Sort](https://www.tutorialspoint.com/tutorial_view.php?cid=data_structures_algorithms&pid=insertion_sort_algorithm.htm)
    * Day 8
        * [Constructors](http://www.golangpatterns.info/object-oriented/constructors)
    * Day 9
        * [An introduction to using and visualizing channels in Go](https://www.sohamkamani.com/blog/2017/08/24/golang-channels-explained/)
        * [Context in Golang!](https://levelup.gitconnected.com/context-in-golang-98908f042a57)
        * [Go by Example: Context](https://gobyexample.com/context)
    * Day 10 
        * [A UDP server and client in Go](https://ops.tips/blog/udp-client-and-server-in-go/)
        * [Logging in Golang using logrus](https://esc.sh/blog/golang-logging-using-logrus/)
    * Day 11
        * [Write to Client UDP Socket in Go](https://stackoverflow.com/questions/26028700/write-to-client-udp-socket-in-go)
    * Day 12 
        * [A good explanation of regex](https://regex101.com)
        * [Go by Example: Regular Expressions](https://gobyexample.com/regular-expressions)
    * Day 13
        * [Security in Go - Building a Port Scanner in Go](https://tutorialedge.net/projects/building-security-tools-in-go/building-port-scanner-go/)
    * Day 14
        * [Data Structure and Algorithms Selection Sort](https://www.tutorialspoint.com/data_structures_algorithms/selection_sort_algorithm.htm/)
    * Day 15
        * [How to use Lua to expand in golang](https://developpaper.com/how-to-use-lua-to-expand-in-golang/)
        * [gluamapper: maps a GopherLua table to a Go struct](https://github.com/yuin/gluamapper)
        * [Lua - Functions](https://www.tutorialspoint.com/lua/lua_functions.htm)
    * Day 16
        * [Package http](https://golang.org/pkg/net/http/)
        * [Go maps in action](https://blog.golang.org/maps)
        * [GoLang : Dynamic JSON Parsing using empty Interface and without Struct in Go Language](https://irshadhasmat.medium.com/golang-simple-json-parsing-using-empty-interface-and-without-struct-in-go-language-e56d0e69968)
    * Day 18
        * [Measuring execution time in a Go program](https://flaviocopes.com/golang-measure-time/)
        * [Multi-thread For Loops Easily and Safely in Go](https://medium.com/@greenraccoon23/multi-thread-for-loops-easily-and-safely-in-go-a2e915302f8b)
    * Day 19 
        * [Ginkgo docs](https://onsi.github.io/ginkgo/#getting-started-writing-your-first-test)
    * Day 20
        * [Reducing nesting in Go functions with early returns](https://danp.net/2015/11/02/reducing-go-nesting.html)
    * Day 22
        * [Embedding Structs in Go with pointer or with value, what is difference between them?](https://ednsquare.com/question/embedding-structs-in-go-with-pointer-or-with-value-what-is-difference-between-them-------WPNBeN)
    * Day 23
        * [Stupid Gopher Tricks](https://talks.golang.org/2015/tricks.slide#1)
    * Day 25
        * [7 Testing Techniques for Your Golang Codebase](https://building.lang.ai/7-testing-techniques-for-your-golang-codebase-77649a96a1c9?gi=7a1dda82b608)
    * Day 26
        * [Codewalk: First-Class Functions in Go](https://golang.org/doc/codewalk/functions/)
        * [Part 33: First Class Functions](https://golangbot.com/first-class-functions/)
    * Day 27 
        * [Go by Example: Channels](https://gobyexample.com/channels)
        * [Go by Example: Channel Buffering](https://gobyexample.com/channel-buffering)
        * [Go by Example: Channel Synchronization](https://gobyexample.com/channel-synchronization)
        * [Go by Example: Channel Directions](https://gobyexample.com/channel-directions)
    * Day 28
        * [Select](https://tour.golang.org/concurrency/5)
    * Day 30
        * [What file extension should be used](https://github.com/qjebbs/vscode-plantuml/issues/138)
        * [How to Replace Characters in Golang String?](https://www.geeksforgeeks.org/how-to-replace-characters-in-golang-string/)
        * [Go Golang, text to speech now!](https://medium.com/@tibor.hegedus/go-golang-text-to-speech-now-c56c7d0addce)
        * [Use Case Diagram](https://plantuml.com/use-case-diagram)

* Portuguese
    * Day 8
        * [Go: Composição vs Herança](https://medium.com/@ViniciusPach_97728/go-composição-vs-herança-2e8b78928c26)
    * Day 9
        * [Entendendo o defer no Go](https://www.digitalocean.com/community/tutorials/understanding-defer-in-go-pt)
    * Day 21      
        * [Ponteiros e erros](https://larien.gitbook.io/aprenda-go-com-testes/primeiros-passos-com-go/ponteiros-e-erros)
    * Day 24
        * [Injeção de dependência](https://larien.gitbook.io/aprenda-go-com-testes/primeiros-passos-com-go/injecao-de-dependencia#escreva-o-teste-primeiro)
        * [Injeção de dependência no ASP.NET Core](https://docs.microsoft.com/pt-br/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.2)
    * Day 25
        * [Mocks](https://larien.gitbook.io/aprenda-go-com-testes/primeiros-passos-com-go/mocks)
    * Day 26
        * [Concorrência](https://larien.gitbook.io/aprenda-go-com-testes/primeiros-passos-com-go/concorrencia)

### Videos
* English
    * Day 3
        * [Interpolation Search algorithm (with Example & CODE)](https://www.youtube.com/watch?v=hF9iJEPegNc)
    * Day 7
        * [Insertion sort in 2 minutes](https://www.youtube.com/watch?v=JU767SDMDvA)
    * Day 8
        * [GOLANG OOP - COMPOSITION IN GO](https://www.youtube.com/watch?v=194blNHDdd0)
    * Day 14         
        * [Selection sort in 3 minutes](https://www.youtube.com/watch?v=g-PGLbMth_g)
    
* Portuguese
    * Day 3
        * [Busca por Interpolação - Introdução e Implementação](https://www.youtube.com/watch?v=fYbBukE0lTk)
    * Day 6
        * [Bubble sort in 2 minutes](https://www.youtube.com/watch?v=xli_FI7CuzA)
    * Day 15
        * [Estendendo Lua com Golang](https://www.youtube.com/watch?v=kijpLAZMkgM)
    