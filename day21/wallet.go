package day21

import "fmt"

type Wallet struct {
	value int
}

func New(initialValue int) *Wallet {
	return &Wallet{initialValue}
}

func (w *Wallet) Deposit(value int) error {
	if value <= 0 {
		return fmt.Errorf("Please use more money oO")
	}
	w.value += value
	return nil
}

func (w *Wallet) Balance() int {
	return w.value
}
