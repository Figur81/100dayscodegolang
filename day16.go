package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

func getPokemonName(name string) interface{} {
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/" + name)
	if err != nil {
		logrus.Info("failed to make http get: ", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Info("failed to parse received body: ", err)
	}
	result := make(map[string]interface{})
	json.Unmarshal([]byte(string(body)), &result)
	return result["name"]
}

func main() {
	fmt.Println(getPokemonName("ditto"))
}
