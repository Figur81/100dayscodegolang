package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/hajimehoshi/go-mp3"
	"github.com/hajimehoshi/oto"
	htgotts "github.com/hegedustibor/htgo-tts"
	"github.com/sirupsen/logrus"
)

func run(phrase string) error {
	file, err := os.Open(fmt.Sprintf("./audio/%s.mp3", phrase))
	if err != nil {
		return err
	}
	defer file.Close()
	mp3Decoder, err := mp3.NewDecoder(file)
	if err != nil {
		return err
	}

	playerContext, err := oto.NewContext(mp3Decoder.SampleRate(), 2, 2, 8192)
	if err != nil {
		return err
	}
	defer playerContext.Close()

	player := playerContext.NewPlayer()
	defer player.Close()

	if _, err := io.Copy(player, mp3Decoder); err != nil {
		return err
	}
	return nil
}

func createCompreensiblePhrase(umlExpression string) string {
	compreensiblePhrase := strings.ReplaceAll(umlExpression, "-->", " relate ")
	fmt.Println("after we interpret plant uml")
	fmt.Println(compreensiblePhrase)
	return compreensiblePhrase
}

func main() {
	speech := htgotts.Speech{Folder: "audio", Language: "en"}
	dat, err := ioutil.ReadFile("example.puml")
	if err != nil {
		logrus.Fatal("failed on get plant uml content", err)
	}
	fmt.Println("Plant uml content")
	fmt.Printf("%s \n\n", string(dat))

	phrase := createCompreensiblePhrase(string(dat))
	speech.Speak(phrase)
	run(phrase)
}
