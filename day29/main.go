package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

type config struct {
	Host string
	Port int
}

func main() {
	config := config{}
	file, err := os.Open("./config.json")
	if err != nil {
		logrus.Fatal("Failed to open config file: ", err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		logrus.Fatal("Failed parse config file to struct structure: ", err)
	}
	fmt.Println(config)
}
