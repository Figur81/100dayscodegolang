package main

import (
	"fmt"
	"github.com/fatih/color"
)

func selectionSort(arr []int) {
	totalStepsCount := 0
	internalStepCount := 0
	for indexPointer := 0; indexPointer < len(arr)-1; indexPointer++ {
		fmt.Println("==================================")
		fmt.Println("EXTERNAL STEP: ", indexPointer)
		for comparePointer := indexPointer + 1; comparePointer < len(arr); comparePointer++ {
			fmt.Println(color.YellowString("Internal step: "), internalStepCount)
			swapValues(&arr, comparePointer, indexPointer)
			internalStepCount++
			totalStepsCount++
		}
	}
	fmt.Println(color.GreenString("Result: %v, Total Steps: %d", arr, internalStepCount))
}

func swapValues(arr *[]int, lesser, greater int) {
	if (*arr)[lesser] < (*arr)[greater] {
		(*arr)[lesser], (*arr)[greater] = (*arr)[greater], (*arr)[lesser]
	}
}

func main() {
	selectionSort([]int{14, 33, 27, 10, 35, 19, 42, 44})
}
