package main

import (
	"fmt"
	"log"
)

func linearSearch(array []int, valueData int) (response string, err error) {
	fmt.Println("Linear search")
	for index, arrayValue := range array {
		fmt.Println(index)
		if arrayValue == valueData {
			return fmt.Sprintf("arrayValue: %d index: %d \n", arrayValue, index), nil
		}
	}
	return "", fmt.Errorf("failed to find arrayValue")
}

func iterativeBinarySearch(array []int, valueData int) (response string, err error) {
	begin := 0
	end := len(array) - 1

	fmt.Println("Iterative binary search: ")
	for begin <= end {
		index := (begin + end) / 2
		fmt.Println(index)
		if array[index] == valueData {
			return fmt.Sprintf("value: %d index: %d \n", array[index], index), nil
		}
		if array[index] < valueData {
			begin = index + 1
		} else {
			end = index
		}
	}
	return "", fmt.Errorf("failed to find value")
}

func showError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func useSearchingTechnique(response string, err error) {
	showError(err)
	fmt.Println(response)
}

func main() {
	array := []int{10, 14, 19, 26, 27, 31, 33, 35, 42, 44, 50, 60, 70, 75, 76, 78, 85, 88, 90}
	useSearchingTechnique(linearSearch(array, 90))
	useSearchingTechnique(iterativeBinarySearch(array, 90))
}
