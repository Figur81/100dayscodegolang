package main

import (
	"fmt"
	"log"
)

type instanceSearch struct {
	array                      []int
	searchValue                int
	actualMid                  int
	maximumIndex, minimumIndex *int
}

func updateSearchRange(instance instanceSearch) {
	if instance.array[instance.actualMid] < instance.searchValue {
		nextValue := instance.actualMid + 1
		instance.minimumIndex = &nextValue
	} else {
		nextValue := instance.actualMid - 1
		instance.maximumIndex = &nextValue
	}
}

func interpolationSearch(array []int, searchValue int) (response string, err error) {
	minimumIndex := 0
	maximumIndex := len(array) - 1

	for minimumIndex <= maximumIndex {
		proportionalMid := minimumIndex + ((maximumIndex-minimumIndex)/(array[maximumIndex]-array[minimumIndex]))*(searchValue-array[minimumIndex])
		fmt.Printf("actual mid: %d \n", proportionalMid)
		if array[proportionalMid] == searchValue {
			return fmt.Sprintf("value: %d index: %d \n", array[proportionalMid], proportionalMid), nil
		} else {
			updateSearchRange(instanceSearch{array, searchValue, proportionalMid, &maximumIndex, &minimumIndex})
		}
	}
	return "", fmt.Errorf("failed to find value")
}

func main() {
	resultMessage, err := interpolationSearch([]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 8)
	if err != nil {
		if err != nil {
			log.Fatal(err)
		}
	}
	fmt.Println(resultMessage)
}
