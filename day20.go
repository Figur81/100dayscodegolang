package main

import (
	"github.com/sirupsen/logrus"
	"net/http"
)

func requestGoogle() {
	response, err := http.Get("https://google.com")
	if err != nil {
		logrus.Fatal("Failed to make GET on google")
	}
	if response.StatusCode != 200 {
		logrus.Warn("Some problem occurrs on this communication")
		return
	}
	logrus.Info("Successful request")
}

func main() {
	requestGoogle()
}
