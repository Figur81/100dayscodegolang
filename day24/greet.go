package main

import (
	"fmt"
	"io"
)

/*
Non Dependency injection case
func Greet(name string) {
	fmt.Printf("Hello, %s", name)
}
*/

func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}
