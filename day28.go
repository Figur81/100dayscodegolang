package main

import (
	"fmt"
)

func fibbonacci(internalChannel chan int, quit chan int) {
	actual, next := 0, 1
	for {
		fmt.Println("actual, next state:", actual, next)
		select {
		case internalChannel <- actual:
			actual, next = next, actual+next
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	channel := make(chan int)
	quit := make(chan int)

	go func() {
		for index := 0; index < 10; index++ {
			fmt.Println("index", index)
			fmt.Println("fibbonacci result: ", <-channel)
		}
		quit <- 0
	}()
	fibbonacci(channel, quit)
}
