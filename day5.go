package main

import "fmt"

func hashDispersal(key, size int) int {
	return key % size
}

func insert(key, data int, dataTable *map[int]int) {
	rangeMaxTable := 20
	hashIndex := hashDispersal(key, rangeMaxTable)
	for (*dataTable)[hashIndex] != 0 && (*dataTable)[hashIndex] != -1 {
		hashIndex = hashIndex + 1
		hashIndex %= rangeMaxTable
	}
	(*dataTable)[hashIndex] = data
}

func search(key int, dataTable *map[int]int) string {
	steps := 1
	rangeMaxTable := 20
	hashIndex := hashDispersal(key, rangeMaxTable)
	_, found := (*dataTable)[hashIndex]
	for found {
		_, isfound := (*dataTable)[hashIndex]
		fmt.Println("Steps to find: ", steps)
		if isfound {
			return fmt.Sprintf("Data search: %d, dataIndex: %d, key: %d\n", (*dataTable)[hashIndex], hashIndex, key)
		} else {
			hashIndex = hashIndex + 1
			hashIndex %= rangeMaxTable
			steps = steps + 1
		}
	}
	return fmt.Sprintf("Failed to find")
}

func main() {
	dataTable := map[int]int{}
	insert(hashDispersal(1, 20), 70, &dataTable)
	insert(hashDispersal(2, 20), 40, &dataTable)
	insert(hashDispersal(42, 20), 30, &dataTable)
	insert(hashDispersal(4, 20), 80, &dataTable)
	fmt.Println(search(42, &dataTable))
	fmt.Print(dataTable)
}
