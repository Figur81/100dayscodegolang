package main

import (
	"github.com/onsi/gomega"
	"testing"
)

func TestDay12(t *testing.T) {
	gomega.RegisterTestingT(t)
	//Valid
	messagesAmount, error := GetAccumulatedMessagesAmount("rice{durable=\"true\",policy=\"unknow\",queue=\"bean\",vhost=\"/\"} 61\n")
	gomega.Expect(error).ShouldNot(gomega.HaveOccurred())
	gomega.Expect(61).Should(gomega.Equal(messagesAmount))
	//Invalid
	_, error = GetAccumulatedMessagesAmount("riasdace{durable=\"true\",policy=\"unknow\",queue=\"bean\",vhost=\"ok\"} 61")
	gomega.Expect(error).Should(gomega.HaveOccurred())
}
