package main

import (
	"context"
	"io"
	"net"
	"os"

	"github.com/sirupsen/logrus"
)

func getUDPServerConnection(address string) *net.UDPConn {
	UDPAddress, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		logrus.Error("Failed to resolve udp address", err)
		os.Exit(1)
	}
	logrus.Infof("Addres %s is valid", address)
	conn, err := net.DialUDP("udp", nil, UDPAddress)
	if err != nil {
		logrus.Errorf("Failed to connect on %s", address)
		os.Exit(1)
	}
	logrus.Infof("Connection stablished on %s", address)
	return conn
}

func communicationErrorHandler(ctx context.Context, doneChan chan error) {
	select {
	case <-ctx.Done():
		logrus.Error("Context was unexpected closed", ctx.Err())
	case err := <-doneChan:
		logrus.Error("aplicattion channel was finished by error:", err)
	}
}

func readPackage(conn *net.UDPConn, doneChan chan error) {
	buffer := make([]byte, 5000000)
	nRead, addr, err := conn.ReadFrom(buffer)
	if err != nil {
		doneChan <- err
		return
	}
	logrus.Infof("packet-received: bytes=%d from=%s\n", nRead, addr.String())
	doneChan <- nil
}

func clientUDP(ctx context.Context, address string, reader io.Reader) {
	conn := getUDPServerConnection(address)
	defer conn.Close()
	doneChan := make(chan error, 1)
	logrus.Info("Prepared to receive packages")
	go func() {
		readPackage(conn, doneChan)
	}()
	communicationErrorHandler(ctx, doneChan)
}

func main() {
	ctx := context.Background()
	var teste io.Reader
	clientUDP(ctx, "127.0.0.1:1234", teste)
}
