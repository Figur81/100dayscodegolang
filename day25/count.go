package day25

import (
	"bytes"
	"fmt"
)

func count(output *bytes.Buffer, sleeper Sleeper) {
	for i := 3; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(output, i)
	}
	sleeper.Sleep()
	fmt.Fprintf(output, "Vai!")
}
