package main

import "fmt"

// print all the array elements one by one
func transverse(array [10]int) {
	fmt.Println("Transverse: ", array)
}

// Adds an element at the given index.
func insertion(array [10]int, valueData, index int) {
	array[index] = valueData
	fmt.Println("Insertion: ", array)
}

// Deletes an element at the given index.
func deletionByIndex(array [10]int, index int) {
	ret := make([]int, 0)
	ret = append(ret, array[:index]...)
	fmt.Println("Deletion: ", append(ret, array[index+1:]...))
}

// Searches an element using the given value.
func searchByValue(array [10]int, valueData int) {
	for _, i := range array {
		if valueData == i {
			fmt.Println("Search by value: ", i)
		}
	}
}

// Searches an element using the given index.
func searchByIndex(array [10]int, valueData int) {
	fmt.Println("Search by index: ", array[valueData])
}

// Updates an element at the given index.
func update(array [10]int, valueData, index int) {
	for i := 0; i < len(array); i++ {
		if index == i {
			array[index] = valueData
			fmt.Println("Update: ", array)
		}
	}
}

func main() {
	array := [10]int{35, 33, 42, 10, 14, 19, 27, 44, 26}
	transverse(array)
	insertion(array, 50, 9)
	deletionByIndex(array, 4)
	searchByValue(array, 35)
	searchByIndex(array, 4)
	update(array, 100, 2)
}
