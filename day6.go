package main

import "fmt"

func swap(array *[]int, index int) {
	(*array)[index], (*array)[index+1] = (*array)[index+1], (*array)[index]
}

func bubbleSort(array []int) {
	sweep := 0
	step := 0
	swapped := true
	for swapped {
		swapped = false
		fmt.Printf("Sweep: %d \n", step)
		for index := 0; index < len(array)-1; index++ {
			if array[index] > array[index+1] {
				swap(&array, index)
				swapped = true
			}
			fmt.Printf("Step: %d, Data status: %v \n", step, array)
			step++
		}
		sweep++
	}
}

func main() {
	bubbleSort([]int{14, 33, 27, 35, 10})
}
