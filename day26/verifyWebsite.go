package main

import "fmt"

type webSiteVerifier func(string) bool

func verifyWebsite(webSiteVerifier webSiteVerifier, urls []string) map[string]bool {
	results := make(map[string]bool)

	for _, url := range urls {
		results[url] = webSiteVerifier(url)
	}

	return results
}

func main() {
	fmt.Print(verifyWebsite(func(url string) bool {
		if url == "waat://furhurterwe.geds" {
			return false
		}
		return true
	}, []string{
		"http://google.com",
		"http://blog.gypsydave5.com",
		"waat://furhurterwe.geds",
	}))
}
